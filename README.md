# InClassGameJam

Say "Jump" for jumping
Press 'Escape' key to pause and unpause

The game is endless (we made Flappy Bird)
The game ends when the player collides with a pipe.

We implemented voice control and procedural generation for the pipes