﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.Windows.Speech;

public class HearVoice : MonoBehaviour
{
    //list of inputs to player controls
    [SerializeField]
    private KeyWordsEvent[] controls;

    private Dictionary<string, UnityEvent> keyWordsEvent;
    private KeywordRecognizer keywordRecognizer;

    void Start()
    {
        FillDictonary();
        keywordRecognizer = new KeywordRecognizer(keyWordsEvent.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += (args) =>
        {
            keyWordsEvent[args.text]?.Invoke();
        };
        keywordRecognizer.Start();
    }

    //fills the dictonary with our controls
    private void FillDictonary()
    {
        keyWordsEvent = new Dictionary<string, UnityEvent>();
        foreach(var item in controls)
        {
            keyWordsEvent[item.Keyword] = item.ActionToDo;
        }
    }
}
