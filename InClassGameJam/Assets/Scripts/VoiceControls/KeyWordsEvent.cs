﻿using UnityEngine.Events;
[System.Serializable]
public class KeyWordsEvent 
{
    //base class used to hold our word events
    public string Keyword;
    public UnityEvent ActionToDo;
}
