﻿using System;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    Rigidbody2D rb2;

    [SerializeField]
    private float moveOffset = 1f;

    public event Action Death;

    void Awake()
    {
        rb2 = GetComponent<Rigidbody2D>();
    }

    public void Jump()
    {
        rb2.velocity = new Vector2(0, moveOffset);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pipe")
        {
            DeathTriggered();
        }
    }

    private void DeathTriggered()
    {
        Death?.Invoke();
    }
}
