﻿//Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

using UnityEngine;

public class PipeMovement : MonoBehaviour
{
    [SerializeField] private Vector3 direction;
    public float speed;

    private void Update()
    {
        Transform myTransform = transform;
        
        Vector3 currentPosition = myTransform.position;
        currentPosition += direction * (speed * Time.deltaTime);

        myTransform.position = currentPosition;
    }
}