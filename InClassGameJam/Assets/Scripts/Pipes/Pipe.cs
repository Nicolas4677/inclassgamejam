﻿//Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

using System;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    [Header("Bounds")]
    [SerializeField] private Transform lowerBound;
    [SerializeField] private Transform upperBound;
    
    [Header("Dimensions")]
    [SerializeField] private float distBetweenPipes = 2f;
    [SerializeField] private float pipeWidth = 1f;
    [SerializeField] private float pipeHeight = 5f;

    public event Action<Pipe> onBecomeInvisible;

    private void Awake()
    {
        SetDistanceBetweenBounds(distBetweenPipes);
        SetPipeDimensions();
    }

    private void SetPipeDimensions()
    {
        lowerBound.localScale = new Vector3(pipeWidth, pipeHeight, 1);
        upperBound.localScale = new Vector3(pipeWidth, pipeHeight, 1);
    }

    public void SetDistanceBetweenBounds(float distance)
    {
        distBetweenPipes = distance;

        float y = distBetweenPipes * 0.5f;
        
        lowerBound.localPosition = new Vector3(0, -y, 0);
        upperBound.localPosition = new Vector3(0, y, 0);
    }

    private void OnBecameInvisible()
    {
        onBecomeInvisible?.Invoke(this);
    }
}