﻿//Copyright (C) 2020, Nicolas Morales Escobar. All rights reserved.

using System;
using UnityEngine;

[System.Serializable]
public class PipeSpawner
{
    [Header("Spawn")] 
    [SerializeField] private float timeBetweenSpawns;
    [SerializeField] private Vector3 spawnPosition;
    [SerializeField] private PipePool pipePool;
    [SerializeField] private Vector2 heightMinMax;

    [Header("Procedural Generation")]
    [SerializeField] private float xFactor = 1.2f;
    [SerializeField] private float yFactor = 1.2f;
    [SerializeField] private int pipeCountForRandom = 5;
    [SerializeField] private float randomMultiplier = 512f;
    
    private int pipeCount;
    
    private float timer;

    public void Initialize()
    {
        pipePool.Initialize();
    }

    public void Execute()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenSpawns)
        {
            SpawnPipe();
            timer = 0f;
        }
    }

    private void SpawnPipe()
    {
        GameObject pipeGO = pipePool.GetInstance();

        Pipe pipe = pipeGO.GetComponent<Pipe>();

        pipe.onBecomeInvisible += RemovePipe;

        Transform pipeT = pipeGO.transform;
        pipeT.position = spawnPosition;
        
        SetPipePosition(pipeT);
        
        pipeGO.SetActive(true);

        pipeCount++;
    }

    private void RemovePipe(Pipe pipe)
    {
        pipe.onBecomeInvisible -= RemovePipe;

        pipePool.Restore(pipe.gameObject);
    }

    private void SetPipePosition(Transform pipeT)
    {
        //Multipliers to modify the randomness
        //The more pipes spawned, the more random the heights become
        float x = xFactor * pipeCount;
        float y = yFactor * pipeCount;
        
        //Pipe with more randomness every pipeContForRandom pipes.
        if (pipeCount % pipeCountForRandom == 0)
        {
            x *= randomMultiplier;
            y *= randomMultiplier;
        }
        
        float value = Mathf.PerlinNoise(x, y);
        float height = heightMinMax.x + (heightMinMax.y - heightMinMax.x) * value;

        Vector3 position = pipeT.position;
        position.y = height;
        pipeT.position = position;
    }
}
