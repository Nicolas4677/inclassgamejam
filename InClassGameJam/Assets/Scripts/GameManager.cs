﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PipeSpawner pipeSpawner;
        [SerializeField] private GameObject loseScreen;

        private PlayerControls playerControls;
        
        private void Awake()
        {
            playerControls = FindObjectOfType<PlayerControls>();
            
            pipeSpawner.Initialize();
        }

        private void OnEnable()
        {
            playerControls.Death += EndGame;
        }

        private void Update()
        {
            pipeSpawner.Execute();
        }

        private void EndGame()
        {
            loseScreen.SetActive(true);
            StartCoroutine(LoseDelay());
        }

        private void OnDisable()
        {
            playerControls.Death -= EndGame;
        }

        private IEnumerator LoseDelay()
        {
            yield return new WaitForSeconds(1f);
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
    }
}